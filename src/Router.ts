import * as http from 'http';

// TODO: Implement this class.
interface Request<T> extends http.IncomingMessage {
    body?: T;
}
export class Router {
    private handlers:{method:HTTPMethod, path:string, handler: Handler<any>}[] = []

    private addRoute<T>(method: HTTPMethod, path: string, handler: Handler<T>): this {
        if (this.handlers.find(handler => handler.method === method && handler.path === path)) {
            throw new Error(`Handler already specified for ${method} ${path}`);
        }
        this.handlers.push({ method, path, handler });
        return this;
    }

    get<T>(path: string, handler: Handler<T>): this {
        return this.addRoute(HTTPMethod.GET,path,handler);
    }

    post<T>(path: string, handler: Handler<T>): this {
        return this.addRoute(HTTPMethod.POST,path,handler);
    }

    put<T>(path: string, handler: Handler<T>): this {
        return this.addRoute(HTTPMethod.PUT,path,handler);
    }

    delete<T>(path: string, handler: Handler<T>): this {
        return this.addRoute(HTTPMethod.DELETE,path,handler);
    }

    getHandler() {
        return async (request: http.IncomingMessage, response: http.ServerResponse) => {
            const method:HTTPMethod = request.method as HTTPMethod
            const url = request.url

            const handler = this.handlers.find(handler=>handler.method === method && handler.path === url)

            if (!handler){
                response.statusCode = 404;
                response.end();
                
                return;
            }
            const req = request as Request<any>;

            try{
                let body = '';
                for await (const chunk of request) {
                    body += chunk;
                }
                req.body = JSON.parse(body)

                await handler.handler(req, response)
            }
            catch{
                console.error(`${method} ${url} internal error`)
                response.statusCode = 500;
                response.end();
                return;
            }
        }
    }
}

export type Handler<T> = (request: any, response: any) => void;

export enum HTTPMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}

